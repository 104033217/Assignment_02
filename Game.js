
let game = new Phaser.Game({
  width: 950,
  height: 600,
  renderer: Phaser.AUTO,
  antialias: true,
  multiTexture: true,
  state: {},
  parent: "canvas"
});
game.state.add("Stage", StageState);
game.state.add("Stage_addChild", StageState_addChild);
game.state.add("Opening", OpeningState);
game.state.start("Opening");
