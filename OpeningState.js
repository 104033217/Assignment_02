var OpeningState = {
  index: 0,
  _UP: 0x0,
  _DOWN: 0x1,
  count: 0,
  precount: 0,

  // phaser object
  op: [],
  background: null,
  key_up: null,
  key_down: null,
  key_z: null,


  indexChange: function(direction){
    let p = this.index;
    if(direction === this._UP){
      this.index--;
      if(this.index < 0)
        this.index = this.op.length - 1;
    }
    else if (direction === this._DOWN){
      this.index++;
      if(this.index >= this.op.length)
        this.index = 0;
    }
    this.indexChanged(p, this.index);
  },
  indexChanged: function(from, to){
    this.op[from].alpha = 0.5;
    this.op[to].alpha = 1;
  },
  keyUpFunction: function(){
    if(this.key_up.justDown){
      this.count = 0;
      this.precount = 0;
      this.indexChange(this._UP);
    }
    this.precount++;
    this.count++;
    if( (this.precount > 30) && (this.count > 6) ){
      this.count = 0;
      this.indexChange(this._UP);
    }
  },
  keyDownFunction: function(){
    if(this.key_down.justDown){
      this.count = 0;
      this.precount = 0;
      this.indexChange(this._DOWN);
    }
    this.precount++;
    this.count++;
    if( (this.precount > 30) && (this.count > 6) ){
      this.count = 0;
      this.indexChange(this._DOWN);
    }
  },
  keyZFunction: function(){
    if(this.index === 0) // start
      game.state.start("Stage");
  },

  initPressKey: function(){
    this.key_up = game.input.keyboard.addKey(38);
    this.key_down = game.input.keyboard.addKey(40);
    this.key_z = game.input.keyboard.addKeys({"z": 122, "Z": 90});
    // console.log(this.key_up);
    // console.log(this.key_down);
    this.key_up.onHoldCallback = this.keyUpFunction.bind(this);
    this.key_down.onHoldCallback = this.keyDownFunction.bind(this);
    this.key_z.Z.onHoldCallback = this.keyZFunction.bind(this);
  },
  initBG: function(){
    this.background = game.add.image(0, 0, "background");
    let factor = game.height / this.background.height;
    this.background.width *= factor;
    this.background.height *= factor;
    
    this.op[0] = game.add.image(100, 300, "op_start");
    this.op[1] = game.add.image(100, 350, "op_replay");
    this.op[2] = game.add.image(100, 400, "op_option");
    this.op[3] = game.add.image(100, 450, "op_quit");
    this.op[1].alpha = 0.5;
    this.op[2].alpha = 0.5;
    this.op[3].alpha = 0.5;
  },



  preload: function(){
    game.load.image("background", "./img/opening/background.png");
    game.load.image("op_start", "./img/opening/op_start.png");
    game.load.image("op_replay", "./img/opening/op_replay.png");
    game.load.image("op_option", "./img/opening/op_option.png");
    game.load.image("op_quit", "./img/opening/op_quit.png");
  },

  create: function(){
    this.initBG();
    this.initPressKey();
  },

  update: function(){

  },

  render: function(){
    game.debug.text("count: " + this.count, 850, 50, "#eeddff");
    game.debug.text("index: " + this.index, 850, 65, "#eeddff");
    game.debug.text("precount: " + this.precount, 850, 80, "#eeddff");
  }
}