var StageState_addChild = {

  count: 0,
  _fighter: {
    r: 250,
    r_slow: 120,

    flags: 0,
    _FLAG_MOVE_LEFT: 0x1,
    _FLAG_MOVE_RIGHT: 0x2,
    _FLAG_MOVE_DOWN: 0x4,
    _FLAG_MOVE_UP: 0x8,
    _FLAG_SHIFT: 0x10,
    isFlagSet: function(type){
      return (this.flags & type) ? true : false;
    },
    setFlag: function(type){
      let pre = this.isFlagSet(type);
      this.flags |= type;
      return !pre;
    },
    clearFlag: function(type){
      let pre = this.isFlagSet(type);
      this.flags &= ~type;
      return pre;
    }
  },

  // phaser object
  cursor: null,
  fighter: null,
  slow_dot: null,
  fighterGroup: null,

  key_shift: null,
  key_left: null,
  key_right: null,
  key_down: null,
  key_up: null,


  updatePlayer: function() {
    var shift = this._fighter._FLAG_SHIFT;
    if( !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_LEFT ) &&
        !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_RIGHT ) ){
      this.fighter.body.velocity.x = 0;
    }
    if( !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_DOWN ) &&
        !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_UP ) ){
      this.fighter.body.velocity.y = 0;
    }

    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_LEFT ) ){
      this.fighter.body.velocity.x = 
        this._fighter.isFlagSet( shift ) ? - this._fighter.r_slow : - this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_RIGHT ) ){
      this.fighter.body.velocity.x = 
        this._fighter.isFlagSet( shift ) ? this._fighter.r_slow : this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_DOWN ) ){
      this.fighter.body.velocity.y = 
        this._fighter.isFlagSet( shift ) ? this._fighter.r_slow : this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_UP ) ){
      this.fighter.body.velocity.y = 
        this._fighter.isFlagSet( shift ) ? - this._fighter.r_slow : - this._fighter.r;
    }

    
  },

  handleKey: function(){
    if( this.key_left.justDown ){
      this._fighter.setFlag( this._fighter._FLAG_MOVE_LEFT );
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_RIGHT );
      this.fighter.animations.play("move_left");
    }
    if( this.key_right.justDown ){
      this._fighter.setFlag( this._fighter._FLAG_MOVE_RIGHT );
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_LEFT );
      this.fighter.animations.play("move_right");
    }
    if( this.key_down.justDown ){
      this._fighter.setFlag( this._fighter._FLAG_MOVE_DOWN );
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_UP );
    }
    if( this.key_up.justDown ){
      this._fighter.setFlag( this._fighter._FLAG_MOVE_UP );
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_DOWN );
    }

    if( this.key_left.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_LEFT );
      this.fighter.animations.play("stand");
      if( this.key_right.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_RIGHT );
        this.fighter.animations.play("move_right");
      }
    }
    if( this.key_right.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_RIGHT );
      this.fighter.animations.play("stand");
      if( this.key_left.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_LEFT );
        this.fighter.animations.play("move_left");
      }
    }
    if( this.key_down.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_DOWN );
      if( this.key_up.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_UP );
      }
    }
    if( this.key_up.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_UP );
      if(this.key_down.isDown){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_DOWN );
      }
    }
    
    if( this.key_shift.justDown ){
      this._fighter.setFlag( this._fighter._FLAG_SHIFT );
    }
    if( this.key_shift.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_SHIFT );
    }
  },
    

    



  preload: function() {
    game.load.spritesheet("fighter", "./assets/stage/reimu.png", 32, 48);
    game.load.image("slow_dot", "./assets/stage/slow_dot.png");
  },

  create: function() {
    // init key
    this.key_left = game.input.keyboard.addKey(37);
    this.key_right = game.input.keyboard.addKey(39);
    this.key_down = game.input.keyboard.addKey(40);
    this.key_up = game.input.keyboard.addKey(38);
    this.key_shift = game.input.keyboard.addKey(16);
        
    this.fighter = game.add.sprite( game.width/3 , 500 , "fighter");
    this.fighter.animations.add("stand",      [  0,  1,  2,  3,  4,  5,  6,  7], 16, true);
    this.fighter.animations.add("move_left",  [  8,  9, 10, 11, 12, 13, 14, 15], 32, false);
    this.fighter.animations.add("move_right", [ 16, 17, 18, 19, 20, 21, 22, 23], 32, false);
    this.fighter.animations.play("stand");

    // issue: slow_dot's position related to parent is the detached slow_dot.body's postion
    // issue: ↑↑↑ this.fighter.addChild(0, 0, this.slow_dot); ↑↑↑
    this.slow_dot = this.fighter.addChild( game.make.sprite( 100 , 100 , "slow_dot" ) );
    // this.slow_dot = game.add.sprite( 100 , 100 , "slow_dot" );

    // physics setting
    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.enable(this.fighter, true);  // use "true" to enable debug drawing
    // this.fighter.body.clearShapes();
    game.physics.p2.enable(this.slow_dot, true);
    
    // this.fighter.body.clearShapes();
    this.fighter.body.fixedRotation = true;
    
    this.slow_dot.body.collideWorldBounds = true;
    this.slow_dot.body.fixedRotation = true;  // unable rotate 
    this.slow_dot.body.clearShapes();  // clear shapes for collision
    this.slow_dot.body.addCircle(4, 0, 0);

    
    game.time.advancedTiming = true;  // for game.time.fps
    
  },
    
    update: function() {
      this.count = (this.count + 1) % 50;

      this.handleKey();
      this.updatePlayer();
    },
    
    render: function(){
      // issue: body info only support arcade and box2d
      // ref: https://github.com/photonstorm/phaser/blob/v2.6.2/src/utils/Debug.js
      
      game.debug.text("slow_dot.coor: " + (prex - this.slow_dot.body.x) + " " + (prey - this.slow_dot.body.y) , 1, 10);
      prex = this.slow_dot.body.x;
      prey = this.slow_dot.body.y;
      game.debug.text("fps: " + game.time.fps, 900, 15, "#eeddff");
    }
};

var prex = 0;
var prey = 0;