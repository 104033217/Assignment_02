
// todo: different sound effects on enemy shots

function EnemyMgr( stage ){
  console.log("StageState: ", stage);
  this.stage = stage;
  this.enemyIndex = 0;
  // note if the object entries are serially packed to array
  // if not, we just change to manually push() enemy data;
  this.enemies = Object.values( enemy_params );
}


EnemyMgr.prototype.checkSpawn = function(){
  if( this.enemyIndex >= this.enemies.length ){
    return;
  }
  if( this.stage._count >= this.enemies[ this.enemyIndex ].count ){
    this.createEnemy();
    this.enemyIndex++;
    // while( !this.enemies[ this.enemyIndex ].count && this.enemyIndex < this.enemies.length ){  // boss的第二階段以後不會有count
    //   this.enemyIndex++;
    // }
  }
}

EnemyMgr.prototype.createEnemy = function(){
  let params = this.enemies[ this.enemyIndex ].params;
  let e = this.stage.enemyGroup.create( params.spawn.x, params.spawn.y, params.type );
  e._vIndex     = -1;
  e._count      = 0;
  e._lifecount  = params.lifecount;
  e._hp         = params.hp;
  e._s          = [];
  e._v          = [];
  e._v_orig     = [];  // for reset
  e._shotIndex  = 0;
  e._items      = params.item;
  e._stage      = this.stage;
  e._enemyMgr   = this;
  if( typeof params.boss === "number" ){
    e._boss     = params.boss;
  }
  
  // we update enemy's v , so v should be deep copied to ensure original params unchanged
  params.v.forEach((v, i) => {
    e._v.push( { ...v } );
    e._v_orig.push( { ...v } );
    // adjustment
    if( typeof v.theta === "number" ){
      e._v[i].theta += -90;
      e._v_orig[i].theta += -90;
    }
  });
  params.s.forEach((s, i) => {
    e._s.push( { ...s } );
  })


  e._runstep = function(){
    this._count++;
    if( this._count > this._lifecount ){
      this.destroy();
      // console.log(this.x, this.y);
      return;
    }

    this._updateVector();
    if( this._checkVectorChange() ){
      this._changeVector();
    }

    // 目前允許同一個count最多射四發
    let shot = this._s[ this._shotIndex ];
    if( this._shotIndex < this._s.length && this._count >= shot.count ){
      this._shot( shot );

      shot = this._s[ this._shotIndex ];
      if( this._shotIndex < this._s.length && this._count >= shot.count ){
        this._shot( shot );

        shot = this._s[ this._shotIndex ];
        if( this._shotIndex < this._s.length && this._count >= shot.count ){
          this._shot( shot );

          shot = this._s[ this._shotIndex ];
          if( this._shotIndex < this._s.length && this._count >= shot.count ){
            this._shot( shot );
          }
        }
      }
    }

  }

  e._checkVectorChange = function(){
    if( this._vIndex >= this._v.length - 1 ){
      return false;
    }
    if( this._count >= this._v[ this._vIndex + 1 ].count ){
      return true;
    }
    return false;
  }

  e._changeVector = function(){
    let prev = this._v[ this._vIndex ];
    let prev_orig = this._v_orig[ this._vIndex ];
    
    this._vIndex++;
    if( this._vIndex === 0 ){  // first vector
      this.body.velocity.x = cal_rx( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
      this.body.velocity.y = cal_ry( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
      return;
    }
    
    // issue: bug: jump to index 0
    // (if) jump to another vector
    if( this._v[ this._vIndex ].index ){
      let shotRollback = this._v[ this._vIndex ].shotRollback

      // note the count will also be set to that vector
      this._vIndex = this._v[ this._vIndex ].index;
      this._count = this._v[ this._vIndex ].count;
      if( shotRollback ){
        // find the nearest shot of the rollbacked _count
        let counts = this._s.map((shot) => {
          return shot.count - this._count;
        });
        for(let index = 0; index < counts.length; index++){
          if( counts[index] >= 0 ){
            this._shotIndex = index;
            break;
          }
        }
      }
      
    }

    // whether inheritance the previous vector's final speed
    if( typeof this._v[ this._vIndex ].r === "string" ){
      this._v[ this._vIndex ].r = Math.abs( prev.r );
    }
    if( typeof this._v[ this._vIndex ].theta === "string" ){
      this._v[ this._vIndex ].theta = prev.theta;
    }
    
    
    if( prev && prev.debug ){
      console.log( this.body.x, this.body.y );
    }
    
    // reset previous vector
    if( prev ){
      prev.r     = prev_orig.r;
      prev.theta = prev_orig.theta;
      prev.ra    = prev_orig.ra;
      prev.w     = prev_orig.w;
      prev.raa     = prev_orig.raa;
      prev.wa     = prev_orig.wa;
    }
    
    this.body.velocity.x = cal_rx( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
    this.body.velocity.y = cal_ry( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );

  }

  e._updateVector = function(){
    this._v[ this._vIndex ].r     += this._v[ this._vIndex ].ra;
    this._v[ this._vIndex ].ra    += this._v[ this._vIndex ].raa;
    this._v[ this._vIndex ].theta += this._v[ this._vIndex ].w;
    this._v[ this._vIndex ].w     += this._v[ this._vIndex ].wa;
    this.body.velocity.x = cal_rx( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
    this.body.velocity.y = cal_ry( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
  }

  e._shot = function( shot ){
    EnemyBulletMgr.create( this, shot );
    this._stage.se( 0, shot.se_depress );
  }

  e._taosareru = function(){
    this._createItem();
    if( this._boss === 1 ){
      this._stage.se(8);
      EnemyBulletMgr.bulletGroupArray.forEach((bulletGroup) => {
        bulletGroup.callAllExists("_kieru", true);
      })
    }
    else if ( this._boss === 2 ){
      this._stage.se(9);
      setTimeout(() => { this._stage.pass(); }, 3000);
      EnemyBulletMgr.bulletGroupArray.forEach((bulletGroup) => {
        bulletGroup.callAllExists("_kieru", true);
      })
    }
    else {
      this._stage.se(1);
      this._stage.emitter_etama2.x = this.x;
      this._stage.emitter_etama2.y = this.y;
      this._stage.emitter_etama2.start(true, 180, null, 1);
    }
  }

  e._createItem = function(){
    e._items.forEach((item) => {
      this._stage.createItem( 
        this.body.x + Math.random() * 30 - 15, 
        this.body.y + Math.random() * 30 - 15, 
        item );
    });
  }

  e._changeVector();  // beforefirst(-1) -> next(0)
  e.body.data.collisionResponse = false;
  e.body.setZeroDamping();
  // e.body.debug = true;

  e.body.setCollisionGroup( this.stage.enemyCollisionGroup );
  // collision group pair should set collides() on two groups each other
  e.body.collides( [ this.stage.fighterBulletCollisionGroup, this.stage.fighterCollisionGroup, this.stage.fighterBombCollisionGroup ] );
  e.body.onBeginContact.add(function(data1, data2){
    if( data2.type === 2 ) return;
    if( data2._name === "slow_dot" && !this._stage._fighter.isFlagSet( this._stage._fighter._FLAG_INVINCIBLE ) ){
      this._stage._fighter.hit();
      return;
    }
    this._hp--;
    if( data2._name === "fighter_bomb" ){
      this._hp -= 5;
    }
    if( this._hp <= 0 ){
      if( this._boss === undefined || this._boss === 2 ){
        this._taosareru();
        this.destroy();
      }
      else if ( this._boss === 1 ) {
        this._taosareru();
        this._enemyMgr.createEnemy();
        this._enemyMgr.enemyIndex++;
        this.destroy();
      }
    }
  }, e);

  
}



var EnemyBulletMgr = {
  bulletGroupArray: [],

  create: function( enemy, shot ){

    let [ x, y ] = [ enemy.body.x, enemy.body.y ];
    
    if( shot.offset_r && shot.offset_r_t ){
      // calculate x, y offset
      x += cal_rx( shot.offset_r, shot.offset_r_t - 90 );  // -90: transform to phaser's theta
      y += cal_ry( shot.offset_r, shot.offset_r_t - 90 );
    }

    let baseAngle = -90;  // -90: adjustmnet..., phaser's default angle is strange(to left)
    if( shot.aiming ){
      let a = Phaser.Math.angleBetween( x, y, StageState.slow_dot.body.x, StageState.slow_dot.body.y );
      baseAngle += a / Math.PI * 180 - 90;  // -90: adjustment............
    }
    if( typeof shot.offset_t === "number" ){
      // calculate theta offset;
      baseAngle += shot.offset_t;
    }
    else if ( typeof shot.offset_t === "string" && shot.offset_t === "r" ){
      baseAngle += Math.random() * 360 - 180;
    }
    else if ( typeof shot.offset_t === "string" && shot.offset_t === "rs" ){
      baseAngle += Math.random() * 20 - 10;
    }
  
    let n = enemy_bullets[ shot.bullet ].length - 1;
    let b;
    for(; n >= 0 ; n--){
      b = this.bulletGroupArray[ shot.type ].getFirstDead();
      if( !b ){
        console.log("你的子彈存量夠嗎?");
        return;
      }
      b._count  = 0;
      b._vIndex = -1;
      b.frame   = shot.color;
      b._grazed = false;
      b._stage = enemy._stage;
      b._v = [];
  
      // deep copy vectors
      enemy_bullets[ shot.bullet ][ n ].v.forEach((vector) => {
        b._v.push( { count: vector.count, 
                     r:     vector.r, 
                     theta: vector.theta + baseAngle,
                     ra:    vector.ra, 
                     w:     vector.w, 
                     raa:   vector.raa, 
                     wa:    vector.wa } );
      });

      etest = b;
  
      b._currentVector = null;
      b._runstep = function(){
        this._count++;
        this._updateVector();
        if( this._checkVectorChange() ){
          this._changeVector();
        }
      }
      b._checkVectorChange = function(){
        if( this._vIndex >= this._v.length - 1 ){
          return false;
        }
        if( this._count >= this._v[ this._vIndex + 1 ].count ){
          return true;
        }
        return false;
      }
      b._changeVector = function(){
        let prev = this._v[ this._vIndex ];
        this._vIndex++;
        // whether inheritance the previous vector's final speed
        if( typeof this._v[ this._vIndex ].r === "string" ){
          this._v[ this._vIndex ].r = prev.r;
        }
        if( typeof this._v[ this._vIndex ].theta === "string" ){
          this._v[ this._vIndex ].theta = prev.theta;
        }

        this.body.velocity.x = cal_rx( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
        this.body.velocity.y = cal_ry( this._v[ this._vIndex ].r , this._v[ this._vIndex ].theta );
      }
      b._updateVector = function(){
        this._v[ this._vIndex ].r     += this._v[ this._vIndex ].ra;
        this._v[ this._vIndex ].ra    += this._v[ this._vIndex ].raa;
        this._v[ this._vIndex ].theta += this._v[ this._vIndex ].w;
        this._v[ this._vIndex ].w     += this._v[ this._vIndex ].wa;
        this.angle = this._v[ this._vIndex ].theta - 90;  // -90: adjustment for bullets having direction
        this.body.angle = this.angle;
        this.body.velocity.x = cal_rx( this._v[ this._vIndex ].r, this._v[ this._vIndex ].theta);
        this.body.velocity.y = cal_ry( this._v[ this._vIndex ].r, this._v[ this._vIndex ].theta);
      }

      b._kieru = function(){
        if( Math.random() > 0.5 ){
          this._stage.createItem( this.x, this.y, "s" );
        }  
        this.kill();
      }


      b.reset( x , y );
      b._changeVector();
      // b.body.debug = true;  // check if body rotates with renderer(image)
    }
  
    
    enemy._shotIndex++;
  }

} 

var etest;