## Software Studio 2019 Spring Assignment 2 danmaku game 104033217
## 設計模板

*  #### 東方project  
> 以獨特的音樂和奇葩的彈幕造型聞名的獨立遊戲  
> 參考影片：<a href="https://www.youtube.com/watch?v=TZr8g_UYGRc">東方風神録 Lunatic ノーミスノーボムフルスペカ 霊夢B</a>  
*  #### <a href="https://github.com/takahirox/toho-like-js" >takahirox/toho-like-js</a>  
> 在寫作業之前，我完全沒有頭緒，直到我找到了這個project，並trace了他的code  
> 這次作業有部分架構是取自這個source  
> 不過到最後code還是混在一起了，到底有甚麼架構啊QQ  

## 操作簡介
#### menu
- ↑↓, Z
- 第一個start開始遊戲
- 其他三個選項是裝飾品

#### stagestate
- 移動
  - ↑↓←→ [ + shift ]
- 射擊
  - Z
  - 射擊範圍在不同power區間會有變化
- Bomb
  - X
  - 消耗1 power，power 低於2.5時無法使用
- 暫停
  - esc
- for test
  - Q
  - W
- 給第一次接觸這份作業的玩家
  - 自機的碰撞判定只有中央的小圓圈
  - 擦彈有額外分數
  - 重新進入遊戲(main state -> stage state)，分數、生命值等資料不會重置，此為正常現象，請放心使用
  - 沒有BGM是正常現象，我沒有放BGM
  
## demo
<a href="https://104033217.gitlab.io/Assignment_02">https://104033217.gitlab.io/Assignment_02</a>  
碰撞關係圖：  
<img src="./collision_relationship.png" width="600" height="auto"> <img src="./demo.png" width="500" height="auto">  
todo: 自動導向(敵機彈/自機彈)

## Score
|Component|Score|Done|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|
|Appearance(subjective)|5%|Y|

## Bonus
- 使用P2物理引擎
  - 可以偵測圓形、多邊形的接觸
- slow mode
  - 按住shift，移動會變慢，進行「精密的避彈」(<a href="https://zh.wikipedia.org/wiki/%E5%BD%88%E5%B9%95%E5%B0%84%E6%93%8A%E9%81%8A%E6%88%B2#%E7%89%B9%E5%BE%B5">wiki: 彈幕射擊遊戲</a>)
  - 自機碰撞判定點會出現
- 擦彈機制(graze)
  - 敵機子彈離自機夠進時判斷為擦彈
  - 可以獲得額外分數
  - 同一顆子彈不重複計算擦彈
  - 其實實作並不是用距離判斷
- 無敵狀態
  - 自機復活後會有一段無敵時間
- **關卡容易擴充**
  - 雖然剛PO上這作業時遊戲內容還很短，但是只要更改兩個參數檔案(子彈參數、敵機參數)就能擴充遊戲內容，希望大家能自由發揮
  - `enemy_bullets_params.js`定義各個子彈的移動路徑(v, vector)
    - count:  number -> 時間點
    - r: number | string -> 速度 (string -> 繼承上個vector)
    - theta: number | string -> 角度 (順時針為正方向，同phaser) (string -> 繼承上個vector)
    - ra: number -> 加速度
    - w: number -> 角速度
    - raa: number -> 加加速度
    - wa: number -> 角加速度
  - `enemy_params.js`定義敵機的參數
    - count: number -> 登場時間
    - type: string ->sprite(image)種類
    - lifecount: number -> 生命週期
    - hp: number -> hp
    - spawn: {x: number, y: number} -> 出生地點
    - item: [string, string, ....] -> 掉落物 (string = "p" | "t" | "P" | "1up" | "s")
    - s: [] -> 使用哪些子彈
      - count: number -> 發射時機
      - type: number -> 彈種
      - color: number -> 對應到spritesheet的index
      - bullet: number -> 對應到`enemy_bullets_params.js`裡面的index
      - aiming: boolean -> 是否瞄準玩家
      - offset_t: number | string -> 指定額外的角度offset (string = ["r", random在-180~180度] | ["rs", random在-10~10度]) (option)
      - offset_r: number -> 指定發射子彈的位置(半徑)offset (option)
      - offset_r_t: number -> 指定發射子彈的位置(角度)offset (option)
    - v: 定義敵機移動路徑
      - option1: 移動路徑vector，參數同上面的`enemy_bullets_params.js`，或是
      - option2: 跳到先前的vector，需指定以下參數
        - index: number -> 目標vector的index
        - shotRollback: boolean -> 是否根據目標vector的count重新發射子彈 (option)
  - 基本上只要改這兩個參數檔就可以對關卡內容自由更改
    - BUG有，但我祈禱他不要發生
    - 盡量符合參數要求，注意某些參數相依性，第一次改時先注意count順序不要亂掉QQ
  - 目前Boss的stage3使用了兩個比較複雜的子彈
    - 第二個是第一個的反方向旋轉，只差了一個負號
    - 這兩個子彈佔了`enemy_bullets_params.js`3500行(80%)，應該要用迴圈產生比較好
    - 畫面上大概會有600個子彈，fps很有可能會降低

## resources
- opening/background.png
- opening/op_option.png
- opening/op_quit.png
- opening/op_replay.png
- opening/op_start.png
- stage/bomb.png
- stage/end.png
- stage/front.png
- stage/panel.png
- stage/pause.png
- stage/particle_for_as.png
  - me  
---
- stage/bulletE_shape.json
- stage/bulletF_shape.json
- stage/bulletG_shape.json
- stage/bulletH_shape.json
- stage/bulletI_shape.json
  - PhysicsEditor & me
---
- stage/effect_tiny.png
- stage/enemyA.png
- stage/enemyB.png
- stage/enemyC.png
- stage/enemyD.png
- stage/fighter_bullet.png
- stage/reimu.png
- stage/dot_eirin.png
- stage/shot_all_A.png
- stage/shot_all_B.png
- stage/shot_all_C.png
- stage/shot_all_D.png
- stage/shot_all_E.png
- stage/shot_all_F.png
- stage/shot_all_G.png
- stage/shot_all_H.png
- stage/shot_all_I.png
- stage/shot_all_J.png
- stage/slow_dot.png
- stage/stage04b.png
- stage02b.png
- stage/th10.png
- stage/enemy_vanish.wav
- stage/se_graze.wav
- stage/se_item00.wav
- stage/se_pldead00.wav
- stage/se_plst00.wav
- stage/se_powerup.wav
- stage/se_slash.wav
- stage/se_ch00.wav
- stage/se_enep01.wav
- stage/shot1.wav
  - <a href="http://www16.big.or.jp/~zun/">上海アリス幻樂団/ZUN</a>
  - KMAPさん  ドット絵
---
