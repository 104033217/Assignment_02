var StageState = {

  _count: 0,  // need reset when aborting and restart StageState
  _count_start_from: 0,   // for test
  _fighter: {
    r: 280,
    r_slow: 130,
    // r_slow: 30,  // for testing
    power: 3,  // need reset when aborting and restart StageState  // 2 <= power <= 5
    hp: 2,  // need reset when aborting and restart StageState
    graze: 0,  // need reset when aborting and restart StageState
    score: 0,  // need reset when aborting and restart StageState
    invincible_count: 250,

    flags: 0,  // may need reset when aborting and restart StageState
    _FLAG_MOVE_LEFT: 0x1,
    _FLAG_MOVE_RIGHT: 0x2,
    _FLAG_MOVE_DOWN: 0x4,
    _FLAG_MOVE_UP: 0x8,
    _FLAG_SHIFT: 0x10,
    _FLAG_BOMB: 0x20,
    _FLAG_FAIL: 0x40,
    _FLAG_INVINCIBLE: 0x80,

    runstep: function(){
      if( this.isFlagSet( this._FLAG_INVINCIBLE ) ){
        this.invincible_count--;
        if( this.invincible_count < 0 ){
          this.endInvincible();
        }
      }
    },
    isFlagSet: function( type ){
      return ( this.flags & type ) ? true : false;
    },
    setFlag: function( type ){
      let pre = this.isFlagSet( type );
      this.flags |= type;
      return !pre;
    },
    clearFlag: function( type ){
      let pre = this.isFlagSet( type );
      this.flags &= ~type;
      return pre;
    },
    increasePower: function( increment ){
      this.power += increment;
      if( this.power > 5 ){
        this.power = 5;
      }
      if( this.power < 2 ){
        this.power = 2;
      }
      this.power = Number.parseFloat( this.power.toFixed(2) );
    },
    increaseScore: function( increment ){
      this.score += increment;
      this.score = Number.parseInt( this.score );
    },
    increaseHp: function(){
      this.hp++;
    },
    hit: function(){
      this.hp--;
      if( this.hp < 0 ){
        this.setFlag( this._FLAG_FAIL );
        StageState.enterPause();
        return;
      }
      let [ x, y ] = [ StageState.slow_dot.x, StageState.slow_dot.y ];
      for( let i = 8 ; i > 0 ; i-- ){
        StageState.createItem( x + Math.random() * 75 - 25 , y + Math.random() * 75 - 25, "p");
      }
      StageState.createItem( x + Math.random() * 75 - 25 , y + Math.random() * 75 - 25, "P");
      StageState.se(4);
      StageState.slow_dot.reset( 200, 500 );
      StageState.fighter.reset( 200, 500 );
      this.enterInvincible();
      this.increasePower( -1 );
    },
    revive: function(){
      this.hp = 2;
      this.clearFlag( this._FLAG_FAIL );
      StageState.slow_dot.reset( 200, 500 );
      this.enterInvincible();
    },
    enterInvincible: function(){
      this.setFlag( this._FLAG_INVINCIBLE );
      StageState.fighter.alpha = 0.6;
    },
    endInvincible: function(){
      this.clearFlag( this._FLAG_INVINCIBLE );
      StageState.fighter.alpha = 1;
      this.invincible_count = 250;
    }
  },

  // phaser objectssssss
  fighter: null,
  slow_dot: null,
  fighter_bomb: null,
  fighterBulletGroup: null,
  _fighterShotCount: 0,
  createFighterBullet_1: function(){
    let bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.y = -800;  // set body can only after sprite.reset() ????
    // note bullets without setting angle have initial not zero angle but why ? 
  },
  createFighterBullet_1_slow: function(){
    let bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.y = -1200;
  },
  createFighterBullet_2: function(){
    let bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.y = -800;
    
    bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.x = 274;
    bullet.body.velocity.y = -752;
    bullet.body.angle = 20;
    bullet.angle = 20;
    
    bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.x = -274;
    bullet.body.velocity.y = -752;
    bullet.body.angle = -20;
    bullet.angle = -20;
  },
  createFighterBullet_2_slow: function(){
    let bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.y = -1200;
    
    bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.x = 105;
    bullet.body.velocity.y = -1195;
    bullet.body.angle = 5;
    bullet.angle = 5;
    
    bullet = this.fighterBulletGroup.getFirstDead();
    if( !bullet ) return;
    bullet.lifespan = 800;
    bullet.reset( this.fighter.x , this.fighter.y );
    bullet.body.velocity.x = -105;
    bullet.body.velocity.y = -1195;
    bullet.body.angle = -5;
    bullet.angle = -5;
  },
  fighterShot: [
    function(){
      this._fighter.isFlagSet( this._fighter._FLAG_SHIFT ) ? this.createFighterBullet_1_slow() : this.createFighterBullet_1();
      this.se(5);
    },
    function(){
      this._fighter.isFlagSet( this._fighter._FLAG_SHIFT ) ? this.createFighterBullet_2_slow() : this.createFighterBullet_2();
      this.se(5);
    },
    function(){
      this._fighter.isFlagSet( this._fighter._FLAG_SHIFT ) ? this.createFighterBullet_2_slow() : this.createFighterBullet_2();
      this.se(5);
    },
    function(){
      this._fighter.isFlagSet( this._fighter._FLAG_SHIFT ) ? this.createFighterBullet_2_slow() : this.createFighterBullet_2();
      this.se(5);
    }
  ],

  fighterBomb: function(){
    if( this._fighter.power < 2.5 ){
      return;
    }
    this._fighter.increasePower( -1 );
    this.fighter_bomb.reset( this.fighter.body.x, this.fighter.body.y );
    this.fighter_bomb.body.velocity.y = -60;
    this.fighter_bomb.lifespan = 2000;
    this._fighter.setFlag( this._fighter._FLAG_BOMB );
    this.se(2);
  },

  createItem: function( x, y, type ){
    let item
    if( type === "t" ){
      item = this.itemBGroup.getFirstDead();
    }
    else if ( type === "p" ){
      item = this.itemAGroup.getFirstDead();
    }
    else if ( type === "P" ){
      item = this.itemCGroup.getFirstDead();
    }
    else if ( type === "1up" ){
      item = this.itemDGroup.getFirstDead();
    }
    else if ( type === "s" ){
      item = this.itemEGroup.getFirstDead();
    }
    if( !item ){
      return;
    }
    item.lifespan = 5500; // roughly the phaser time of dropping from top to bottom
    item.reset(x, y);
    item.body.velocity.y = -300;
    item.body.velocity.x = 0;
    item._rya = 5;
    item._rya_range = 200;
    item._runstep = function(){
      this.body.velocity.y = Math.min( this.body.velocity.y + this._rya , this._rya_range );
    }
  },

  bulletAGroup: null,
  bulletBGroup: null,
  bulletCGroup: null,
  bulletDGroup: null,
  bulletEGroup: null,
  bulletFGroup: null,
  bulletGGroup: null,
  bulletHGroup: null,
  bulletIGroup: null,
  bulletJGroup: null,
  
  fighterCollisionGroup: null,
  fighterCollisionGroup2: null,
  fighterBulletCollisionGroup: null,
  fighterBombCollisionGroup: null,
  enemyCollisionGroup: null,
  enemyBulletCollisionGroup: null,
  itemCollisionGroup: null,
  
  key_shift: null,
  key_left: null,
  key_right: null,
  key_down: null,
  key_up: null,
  key_z: null,
  key_esc: null,
  
  enemyGroup: null,
  enemyMgr: null,

  se_enemy_taosareru: null,
  se_enemy_shot: null,
  se_fighter_bomb: null,
  se_graze: null,
  se_hit: null,
  se_fighter_shot: null,
  se_item: null,
  se_1up: null,
  se_ch00: null,
  se_enep01: null,

  itemAGroup: null,
  itemBGroup: null,
  itemCGroup: null,
  itemDGroup: null,
  itemEGroup: null,

  panel: null,
  bg: null,
  text_score: null,
  text_hp: null,
  text_power: null,
  text_graze: null,
  text_style: {
    font: "15px Yu Mincho",
    fill: "#dddddd"
  },

  end: null,
  pause_op: [],
  _pause_index: 0,
  paused_key_up_down_function: function(){
    [ this.pause_op[0].alpha, this.pause_op[1].alpha ] = [ this.pause_op[1].alpha, this.pause_op[0].alpha ];
    this._pause_index = (this._pause_index + 1) % this.pause_op.length;
  },
  paused_key_z_function: function(){
    if( this._pause_index === 0 ){
      this.key_esc.onDown.dispatch();
      if( this._fighter.isFlagSet( this._fighter._FLAG_FAIL ) ){
        this._fighter.revive();
      }
    }
    else if ( this._pause_index === 1 ){
      game.paused = false;  // start game loop to start() another state
      game.state.clearCurrentState();
      game.state.start("Opening");
    }
  },
  paused_key_esc_function: function(){
    game.paused = false;
    this.pause_op[0].visible = false;
    this.pause_op[1].visible = false;
    this.key_up.onDown.removeAll();
    this.key_down.onDown.removeAll();
    this.key_z.onDown.removeAll();
    this.key_esc.onDown.removeAll();
  },

  emitter: null,
  emitter_etama2: null,
  emitter_for_as: null,

  preload: function() {
    game.load.spritesheet("fighter", "./img/stage/reimu.png", 32, 48);
    game.load.image("slow_dot", "./img/stage/slow_dot.png");
    game.load.image("fighter_bullet", "./img/stage/fighter_bullet.png");
    game.load.image("fighter_bomb", "./img/stage/bomb.png");
    game.load.image("panel", "./img/stage/panel.png");
    game.load.image("front", "./img/stage/front.png");
    game.load.image("bg", "./img/stage/stage04b.png");
    game.load.image("end", "./img/stage/end.png");

    game.load.spritesheet("bulletA", "./img/stage/shot_all_A.png", 10, 10);
    game.load.spritesheet("bulletB", "./img/stage/shot_all_B.png", 15, 15);
    game.load.spritesheet("bulletC", "./img/stage/shot_all_C.png", 17, 17);
    game.load.spritesheet("bulletD", "./img/stage/shot_all_D.png", 27, 27);
    game.load.spritesheet("bulletE", "./img/stage/shot_all_E.png", 7, 17);
    game.load.spritesheet("bulletF", "./img/stage/shot_all_F.png", 9, 16);
    game.load.spritesheet("bulletG", "./img/stage/shot_all_G.png", 9, 18);
    game.load.spritesheet("bulletH", "./img/stage/shot_all_H.png", 11, 20);
    game.load.spritesheet("bulletI", "./img/stage/shot_all_I.png", 15, 14);
    game.load.spritesheet("bulletJ", "./img/stage/shot_all_J.png", 14, 15);
    game.load.physics("bulletE_shape", "./img/stage/bulletE_shape.json");
    game.load.physics("bulletF_shape", "./img/stage/bulletF_shape.json");
    game.load.physics("bulletG_shape", "./img/stage/bulletG_shape.json");
    game.load.physics("bulletH_shape", "./img/stage/bulletH_shape.json");
    game.load.physics("bulletI_shape", "./img/stage/bulletI_shape.json");

    game.load.image("enemyA", "./img/stage/enemyA.png");
    game.load.image("enemyB", "./img/stage/enemyB.png");
    game.load.image("enemyC", "./img/stage/enemyC.png");
    game.load.image("enemyD", "./img/stage/enemyD.png");
    game.load.image("dot_eirin", "./img/stage/dot_eirin.png");

    game.load.audio("se_enemy_taosareru", "img/stage/enemy_vanish.wav");
    game.load.audio("se_enemy_shot", "img/stage/shot1.wav");
    game.load.audio("se_fighter_bomb", "img/stage/se_slash.wav");
    game.load.audio("se_graze", "img/stage/se_graze.wav");
    game.load.audio("se_hit", "img/stage/se_pldead00.wav");
    game.load.audio("se_fighter_shot", "img/stage/se_plst00.wav");
    game.load.audio("se_item", "img/stage/se_item00.wav");
    game.load.audio("se_1up", "img/stage/se_powerup.wav");
    game.load.audio("se_ch00", "img/stage/se_ch00.wav");
    game.load.audio("se_enep01", "img/stage/se_enep01.wav");

    game.load.spritesheet("pause", "img/stage/pause.png", 187, 50);
    game.load.spritesheet("item", "img/stage/effect_tiny.png", 16, 16);

    game.load.image("leaf", "img/stage/th10.png");
    game.load.image("particle_for_as", "img/stage/particle_for_as.png");
    game.load.image("etama2", "img/stage/etama2.png");
  },

  create: function() {
    // 
    // P2
    game.physics.startSystem(Phaser.Physics.P2JS);
    // 
    // 
    game.world.setBounds(0, 0, 400, 600); // issue: if world.bounds.x or y not 0, it move camera

    // init ui
    this.bg = game.add.sprite(0, 0, "bg");
    game.physics.p2.enable(this.bg, false);
    this.bg.anchor.setTo(0, 1);
    this.bg.body.x = 0;
    this.bg.body.y = 600;
    this.bg.body.clearShapes();
    this.bg.body.setZeroDamping();
    this.bg.body.velocity.y = 20;
    this.panel = game.add.image(400, 0, "panel");
    this.front = game.add.image(420, 50, "front");
    this.front.scale.setTo(0.6, 0.6);
    // note: panel.z is low so you can observe that the { outOfBoundsKill = true } works

    this.initEmitter();
    this._count = this._count_start_from;
    this.enemyMgr = new EnemyMgr( this );
    this.createPhaserObject();
    this.initFighter();
    this.initGroups();

    initTest();
    
    game.time.advancedTiming = true;  // for game.time.fps
  },
    
  update: function() {
    this._count++;

    if(this.bg.body.y > 1624){
      this.bg.body.y = 600;
    }

    this.handleKey();
    this.updatePlayer();
    this.updateInfo();

    this._fighter.runstep();
    this.enemyGroup.callAllExists("_runstep", true);
    EnemyBulletMgr.bulletGroupArray.forEach((bulletGroup) => {
      bulletGroup.callAllExists("_runstep", true);
    })

    this.itemAGroup.callAllExists("_runstep", true);
    this.itemBGroup.callAllExists("_runstep", true);
    this.itemCGroup.callAllExists("_runstep", true);
    this.itemDGroup.callAllExists("_runstep", true);
    this.itemEGroup.callAllExists("_runstep", true);

    this.enemyMgr.checkSpawn();
  },
  
  render: function(){

    game.debug.text("fps: " + game.time.fps, 710, 550, "#eeddff");
    game.debug.text("_count: " + this._count, 710, 570, "#eeddff");
  },












  createPhaserObject: function(){
    // init keyboard
    this.key_left   = game.input.keyboard.addKey(37);
    this.key_right  = game.input.keyboard.addKey(39);
    this.key_down   = game.input.keyboard.addKey(40);
    this.key_up     = game.input.keyboard.addKey(38);
    this.key_shift  = game.input.keyboard.addKey(16);
    this.key_z      = game.input.keyboard.addKey(90);
    this.key_x      = game.input.keyboard.addKey(88);
    this.key_esc    = game.input.keyboard.addKey(27);

    // init sound effects
    this.se_enemy_taosareru = game.add.audio("se_enemy_taosareru");
    this.se_enemy_shot      = game.add.audio("se_enemy_shot");
    this.se_fighter_bomb    = game.add.audio("se_fighter_bomb");
    this.se_graze           = game.add.audio("se_graze");
    this.se_hit             = game.add.audio("se_hit");
    this.se_fighter_shot    = game.add.audio("se_fighter_shot");
    this.se_item            = game.add.audio("se_item");
    this.se_1up             = game.add.audio("se_1up");
    this.se_ch00             = game.add.audio("se_ch00");
    this.se_enep01             = game.add.audio("se_enep01");

    // init pause
    this.end = game.add.image(100, 550, "end");
    this.end.visible = false;
    this.pause_op[0] = game.add.image(100, 250, "pause");
    this.pause_op[1] = game.add.image(100, 300, "pause");
    this.pause_op[0].frame = 0;
    this.pause_op[0].visible = false;
    this.pause_op[1].frame = 1;
    this.pause_op[1].visible = false;

    // init info
    this.text_score = game.add.text(490,  53, this._fighter.score, this.text_style);
    this.text_hp    = game.add.text(490,  77, this._fighter.hp, this.text_style);
    this.text_power = game.add.text(490, 101, this._fighter.power, this.text_style);
    this.text_graze = game.add.text(490, 125, this._fighter.graze, this.text_style);

    // create fighter & slow_dot & bomb
    this.fighter = game.add.sprite( 200 , 500 , "fighter" );
    this.fighter.animations.add("stand",      [  0,  1,  2,  3,  4,  5,  6,  7], 16, true);
    this.fighter.animations.add("move_left",  [  8,  9, 10, 11, 12, 13, 14, 15], 32, false);
    this.fighter.animations.add("move_right", [ 16, 17, 18, 19, 20, 21, 22, 23], 32, false);
    this.fighter.animations.play("stand");
    this.slow_dot = game.add.sprite( this.fighter.x , this.fighter.y , "slow_dot");
    this.fighter_bomb = game.add.sprite( 400, 200, "fighter_bomb" );

    // create groups & collision groups
    this.fighterBulletGroup = game.add.group();
    this.bulletAGroup = game.add.group();
    this.bulletBGroup = game.add.group();
    this.bulletCGroup = game.add.group();
    this.bulletDGroup = game.add.group();
    this.bulletEGroup = game.add.group();
    this.bulletFGroup = game.add.group();
    this.bulletGGroup = game.add.group();
    this.bulletHGroup = game.add.group();
    this.bulletIGroup = game.add.group();
    this.bulletJGroup = game.add.group();
    this.enemyGroup = game.add.group();
    this.itemAGroup = game.add.group();
    this.itemBGroup = game.add.group();
    this.itemCGroup = game.add.group();
    this.itemDGroup = game.add.group();
    this.itemEGroup = game.add.group();
    this.fighterCollisionGroup       = game.physics.p2.createCollisionGroup();
    this.fighterCollisionGroup2      = game.physics.p2.createCollisionGroup();
    this.fighterBulletCollisionGroup = game.physics.p2.createCollisionGroup();
    this.fighterBombCollisionGroup   = game.physics.p2.createCollisionGroup();
    this.enemyCollisionGroup         = game.physics.p2.createCollisionGroup();
    this.enemyBulletCollisionGroup   = game.physics.p2.createCollisionGroup();
    this.itemCollisionGroup          = game.physics.p2.createCollisionGroup();
    //  This part is vital if you want the objects with their own collision groups to still collide with the world bounds
    //  (which we do) - what this does is adjust the bounds to use its own collision group.
    game.physics.p2.updateBoundsCollisionGroup();

  },

  initFighter: function(){
    game.physics.p2.enable(this.fighter, false);  // use "true" to enable debug drawing, anthoer way is { body.bebug = true; }
    game.physics.p2.enable(this.slow_dot, false);
    game.physics.p2.enable(this.fighter_bomb, false);
    
    this.fighter.body.fixedRotation = true;
    this.fighter.body.setCollisionGroup( this.fighterCollisionGroup2 );
    this.fighter.body.collides( [this.itemCollisionGroup, this.enemyBulletCollisionGroup] );
    this.fighter.body.data.collisionResponse = false;
    this.fighter.body.data._name = "fighter";
    
    this.slow_dot.body.fixedRotation = true;
    this.slow_dot.body.clearShapes();
    this.slow_dot.body.addCircle(2, -0.5, -0.5);  // reimu: width = 4, marisa: width = 6
    this.slow_dot.body.data._name = "slow_dot";
    this.slow_dot.visible = false;
    // this.slow_dot.body.collisionResponse = false;
    // this.slow_dot.body.kinematic = true;
    
    // bind position of fighter.body with slow_dot.body ( keypress only changes slow_dot's velocity )
    // with this function, fighter has no need to set collision logic
    game.physics.p2.createLockConstraint( this.fighter.body, this.slow_dot.body );
    this.slow_dot.body.setCollisionGroup( this.fighterCollisionGroup );
    this.slow_dot.body.collides( [ this.enemyCollisionGroup, this.enemyBulletCollisionGroup ] );

    this.fighter_bomb.body.setZeroDamping();
    this.fighter_bomb.body.clearShapes();
    this.fighter_bomb.body.addCircle(200, 0, 0);
    // note: set collision should be after set shape
    this.fighter_bomb.body.setCollisionGroup( this.fighterBombCollisionGroup );
    this.fighter_bomb.body.collides( [ this.enemyBulletCollisionGroup, this.enemyCollisionGroup ] );
    this.fighter_bomb.body.data.collisionResponse = false;
    this.fighter_bomb.body.data._name = "fighter_bomb";
    this.fighter_bomb.kill();
  },

  initGroups: function(){
    this.initBulletGroup(this.fighterBulletGroup, 50, "fighter_bullet");
    // enemy bullet 總數大概 15000 ~ 20000 時效能會明顯下降
    this.initBulletGroup(this.bulletAGroup, 500, "bulletA");
    this.initBulletGroup(this.bulletBGroup, 500, "bulletB");
    this.initBulletGroup(this.bulletCGroup, 500, "bulletC");
    this.initBulletGroup(this.bulletDGroup, 500, "bulletD");
    this.initBulletGroup(this.bulletEGroup, 500, "bulletE");
    this.initBulletGroup(this.bulletFGroup, 500, "bulletF");
    this.initBulletGroup(this.bulletGGroup, 500, "bulletG");
    this.initBulletGroup(this.bulletHGroup, 500, "bulletH");
    this.initBulletGroup(this.bulletIGroup, 500, "bulletI");
    this.initBulletGroup(this.bulletJGroup, 500, "bulletJ");
    this.initItemGroup(this.itemAGroup, 100, 0);
    this.initItemGroup(this.itemBGroup, 100, 1);
    this.initItemGroup(this.itemCGroup, 50, 3);
    this.initItemGroup(this.itemDGroup, 10, 5);
    this.initItemGroup(this.itemEGroup, 1000, 2);  // note fps
    EnemyBulletMgr.bulletGroupArray = [];
    EnemyBulletMgr.bulletGroupArray.push( this.bulletAGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletBGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletCGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletDGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletEGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletFGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletGGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletHGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletIGroup );
    EnemyBulletMgr.bulletGroupArray.push( this.bulletJGroup );
    this.enemyGroup.enableBody = true;
    this.enemyGroup.physicsBodyType = Phaser.Physics.P2JS;
  },

  initBulletGroup: function( bulletGroup, number, type ){
    bulletGroup.enableBody = true;
    bulletGroup.physicsBodyType = Phaser.Physics.P2JS;
    bulletGroup.createMultiple( number, type );
    let bullets = bulletGroup.getAll();
    bullets.forEach(( bullet ) => {
      bullet.body.fixedRotation = true;
      bullet.body.clearShapes();  // load polygon should be after clearshape
      bullet.body.setZeroDamping();
      if( type === "fighter_bullet" ){
        bullet.body.setRectangleFromSprite("fighter_bullet");
        bullet.body.setCollisionGroup( this.fighterBulletCollisionGroup );
        bullet.body.collides( this.enemyCollisionGroup );
        bullet.body.onBeginContact.add(function(data1, data2){
          if( data2.type === 2 ) return;
          bullet.kill();
          this.emitter_for_as.x = bullet.x;
          this.emitter_for_as.y = bullet.y;
          this.emitter_for_as.start(true, 100, null, 10);
        }, this);
      }
      else {
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill  = true;
        // 這裡需要微調offset
        switch( type ){
          case "bulletA":
            bullet.body.addCircle(2, -1, -0.5); break;
          case "bulletB":
            bullet.body.addCircle(4, -0.5, -0.5); break;
          case "bulletC":
            bullet.body.addCircle(4, -0.5, 0); break;
          case "bulletD":
            bullet.body.addCircle(8, -0.5, 0); break;
          case "bulletE":
            bullet.body.loadPolygon("bulletE_shape", "shot_all_E"); break;
          case "bulletF":
            bullet.body.loadPolygon("bulletF_shape", "shot_all_F"); break;
          case "bulletG":
            bullet.body.loadPolygon("bulletG_shape", "shot_all_G"); break;
          case "bulletH":
            bullet.body.loadPolygon("bulletH_shape", "shot_all_H"); break;
          case "bulletI":
            bullet.body.loadPolygon("bulletI_shape", "shot_all_I"); break;
          case "bulletJ":
            bullet.body.addRectangle(6, 8, -0.5, -0.5); break;
        }
        bullet.body.setCollisionGroup( this.enemyBulletCollisionGroup );
        bullet.body.collides( [ this.fighterCollisionGroup, this.fighterBombCollisionGroup, this.fighterCollisionGroup2 ] );
        bullet.body.onBeginContact.add(function(data1, data2){
          if( data2.type === 2 ) return;
          if( data2._name === "fighter_bomb" ){
            bullet.kill();
          }
          if( data2._name === "fighter" ){
            if( !bullet._grazed ){
              this._fighter.graze++;
              this._fighter.increaseScore( Math.random() * 32 + 20 );
              this.se_graze.play();
            }
            bullet._grazed = true;
          }
          if( data2._name === "slow_dot" && !this._fighter.isFlagSet( this._fighter._FLAG_INVINCIBLE ) ){
            this._fighter.hit();
          }
        }, this);
      }

      // 4點了 終於找到這個 配合 Body.onBeginContact.add(callback, context) 達成P2的polygon shape 接觸觸發
      bullet.body.data.collisionResponse = false;

      // bullet.body.debug = true;  // 千萬不要開這個，要開在其他地方開
    });
  },

  initItemGroup: function( itemGroup, number, index ){
    itemGroup.enableBody = true;
    itemGroup.physicsBodyType = Phaser.Physics.P2JS;
    itemGroup.createMultiple( number, "item" );
    let items = itemGroup.getAll();
    items.forEach((item) => {
      item.body.setCollisionGroup( this.itemCollisionGroup );
      item.body.collides( this.fighterCollisionGroup2 );
      item.body.setZeroDamping();
      if( index === 0 ){
        item.body.onBeginContact.add(function(data1, data2){
          if(data2.type === 2) return;
          this._fighter.increasePower( 0.05 );
          if( this._fighter.power === 5 ){
            this._fighter.increaseScore( 50 );
          }
          item.kill();
          this.se(6);
        }, this);
      }
      if( index === 1 ){
        item.body.onBeginContact.add(function(data1, data2){
          if(data2.type === 2) return;
          this._fighter.increaseScore( 150 );
          item.kill();
          this.se(6);
        }, this);
      }
      if( index === 3 ){
        item.body.onBeginContact.add(function(data1, data2){
          console
          if(data2.type === 2) return;
          this._fighter.increasePower( 0.3 );
          if( this._fighter.power === 5 ){
            this._fighter.increaseScore( 300 );
          }
          item.kill();
          this.se(6);
        }, this);
      }
      if( index === 5 ){
        item.body.onBeginContact.add(function(data1, data2){
          if(data2.type === 2) return;
          this._fighter.increaseHp();
          item.kill();
          this.se(7);
        }, this);
      }
      if( index === 2 ){
        item.body.onBeginContact.add(function(data1, data2){
          if(data2.type === 2) return;
          this._fighter.increaseScore( 99 );
          item.kill();
          this.se(6);
        }, this);
      }
      item.frame = index;
      item.body.data.collisionResponse = false;
      item.body.data._name = "item";
    });
  },

  initEmitter: function(){
    this.emitter = game.add.emitter(200, 0, 100);
    this.emitter.width = 400;
    this.emitter.makeParticles("leaf");
    this.emitter.minParticleSpeed.set(-30, 50);
    this.emitter.maxParticleSpeed.set(30, 100);
    this.emitter.setScale(0.2, 1, 0.2, 1, 8000);
    this.emitter.gravity.set(0, 10);
    this.emitter.start(false, 7000, 1000);

    this.emitter_etama2 = game.add.emitter(200, 0, 50);
    this.emitter_etama2.makeParticles("etama2");
    this.emitter_etama2.minParticleSpeed.set(0, 0);
    this.emitter_etama2.maxParticleSpeed.set(0, 0);
    this.emitter_etama2.setScale(0.2, 10, 0.2, 10, 180);
    this.emitter_etama2.gravity.set(0, 0);

    this.emitter_for_as = game.add.emitter(200, 300, 10);
    this.emitter_for_as.width = 10;
    this.emitter_for_as.height = 10;
    this.emitter_for_as.minParticleSpeed.set(-500, -500);
    this.emitter_for_as.maxParticleSpeed.set(500, 500);
    this.emitter_for_as.gravity.set(10, 10);
    this.emitter_for_as.makeParticles("particle_for_as");
    this.emitter_for_as.setScale(0.5, 1, 0.5, 1, 0);
  },

  // issue: in some cases (collision occured), slow_dot detaching with fighter is possible
  updatePlayer: function() {
    var shift = this._fighter._FLAG_SHIFT;
    if( !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_LEFT ) &&
        !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_RIGHT ) ){
      this.slow_dot.body.velocity.x = 0;
    }
    if( !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_DOWN ) &&
        !this._fighter.isFlagSet( this._fighter._FLAG_MOVE_UP ) ){
      this.slow_dot.body.velocity.y = 0;
    }

    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_LEFT ) ){
      this.slow_dot.body.velocity.x = 
        this._fighter.isFlagSet( shift ) ? - this._fighter.r_slow : - this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_RIGHT ) ){
      this.slow_dot.body.velocity.x = 
        this._fighter.isFlagSet( shift ) ? this._fighter.r_slow : this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_DOWN ) ){
      this.slow_dot.body.velocity.y = 
        this._fighter.isFlagSet( shift ) ? this._fighter.r_slow : this._fighter.r;
    }
    if( this._fighter.isFlagSet( this._fighter._FLAG_MOVE_UP ) ){
      this.slow_dot.body.velocity.y = 
        this._fighter.isFlagSet( shift ) ? - this._fighter.r_slow : - this._fighter.r;
    }

    if( this._fighter.isFlagSet( this._fighter._FLAG_BOMB ) ){
      if( !this.fighter_bomb.alive ){
        this._fighter.clearFlag( this._fighter._FLAG_BOMB );
      }
    }
  },
  handleKey: function(){
    if( this.key_left.justDown ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_RIGHT );
      this._fighter.setFlag( this._fighter._FLAG_MOVE_LEFT );
      this.fighter.animations.play("move_left");
    }
    if( this.key_right.justDown ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_LEFT );
      this._fighter.setFlag( this._fighter._FLAG_MOVE_RIGHT );
      this.fighter.animations.play("move_right");
    }
    if( this.key_down.justDown ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_UP );
      this._fighter.setFlag( this._fighter._FLAG_MOVE_DOWN );
    }
    if( this.key_up.justDown ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_DOWN );
      this._fighter.setFlag( this._fighter._FLAG_MOVE_UP );
    }

    if( this.key_left.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_LEFT );
      this.fighter.animations.play("stand");
      if( this.key_right.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_RIGHT );
        this.fighter.animations.play("move_right");
      }
    }
    if( this.key_right.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_RIGHT );
      this.fighter.animations.play("stand");
      if( this.key_left.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_LEFT );
        this.fighter.animations.play("move_left");
      }
    }
    if( this.key_down.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_DOWN );
      if( this.key_up.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_UP );
      }
    }
    if( this.key_up.justUp ){
      this._fighter.clearFlag( this._fighter._FLAG_MOVE_UP );
      if( this.key_down.isDown ){
        this._fighter.setFlag( this._fighter._FLAG_MOVE_DOWN );
      }
    }
    
    if( this.key_shift.justDown ){
      this.slow_dot.visible = true;
      this._fighter.setFlag( this._fighter._FLAG_SHIFT );
    }
    if( this.key_shift.justUp ){
      this.slow_dot.visible = false;
      this._fighter.clearFlag( this._fighter._FLAG_SHIFT );
    }

    if( this.key_z.isDown ){
      if( this.key_z.justDown ){
        this._fighterShotCount = 0;
      }
      if( this._fighterShotCount === 0 ){
        this.fighterShot[ Math.floor(this._fighter.power - 2) ].call(this);
      }
      this._fighterShotCount = (this._fighterShotCount + 1) % 5;
    }

    if( this.key_x.justDown && !this._fighter.isFlagSet( this._fighter._FLAG_BOMB ) ){
      this.fighterBomb();
    }

    // pause
    if( this.key_esc.justDown ){
      this.enterPause();
    }

    // cheat key
    if( this.key_shift.isDown && this.key_esc.isDown ){
      if( !this._fighter.isFlagSet( this._fighter._FLAG_INVINCIBLE ) ){
        this._fighter.enterInvincible();
        this._fighter.invincible_count = 999999;
      }
      else{
        this._fighter.endInvincible();
      }
    }
  },

  updateInfo: function(){
    this.text_score.setText( this._fighter.score );
    this.text_hp.setText( this._fighter.hp );
    this.text_power.setText( this._fighter.power );
    this.text_graze.setText( this._fighter.graze );
  },

  enterPause: function(){
    game.paused = true;
    this.pause_op[0].visible = true;
    this.pause_op[1].visible = true;
    this._fighter.clearFlag( this._fighter._FLAG_MOVE_LEFT );
    this._fighter.clearFlag( this._fighter._FLAG_MOVE_RIGHT );
    this._fighter.clearFlag( this._fighter._FLAG_MOVE_DOWN );
    this._fighter.clearFlag( this._fighter._FLAG_MOVE_UP );
    this._fighter.clearFlag( this._fighter._FLAG_SHIFT );
    this._pause_index = 0;
    this.pause_op[0].alpha = 1;
    this.pause_op[1].alpha = 0.5;

    this.key_esc.onDown.add(this.paused_key_esc_function, this);
    this.key_up.onDown.add(this.paused_key_up_down_function, this);
    this.key_down.onDown.add(this.paused_key_up_down_function, this);
    this.key_z.onDown.add(this.paused_key_z_function, this);
  },

  pass: function(){
    this.end.visible = true;
  },

  se_enemy_shot_count: 0,
  se: function( index, se_enemy_shot_depress = false ){
    switch( index ){
      case 0:
        if( se_enemy_shot_depress ){
          this.se_enemy_shot_count = (this.se_enemy_shot_count + 1) % 5;
          if( this.se_enemy_shot_count === 0 ){
            this.se_enemy_shot.play();
          }
        }
        else{
          this.se_enemy_shot.play();
        }
        break;
      case 1:
        this.se_enemy_taosareru.play(); 
        break;
      case 2:
        this.se_fighter_bomb.play();
        break;
      case 3:
        this.se_graze.play();
        break;
      case 4:
        this.se_hit.play();
        break;
      case 5:
        this.se_fighter_shot.play();
        break;
      case 6:
        this.se_item.play();
        break;
      case 7:
        this.se_1up.play();
        break;
      case 8:
        this.se_ch00.play();
        break;
      case 9:
        this.se_enep01.play();
        break;
    }
  }

};



var test = [];
var key_test1;
var key_test2;
var inin = false;
function initTest(){
  let textStyle = {
    font: "14px Yu Mincho",
    fill: "#ffffff"
  }
  game.add.text(410, 350, "操控：", textStyle);
  game.add.text(410, 370, "↑↓←→ [ + shift ]", textStyle);
  game.add.text(410, 390, "Z", textStyle);
  game.add.text(410, 410, "X", textStyle);
  game.add.text(410, 430, "Esc", textStyle);

  game.add.text(410, 480, "這裡列出了目前所有的子彈類型", textStyle);
  game.add.text(410, 500, "你可以", textStyle);
  game.add.text(410, 520, "Q: 開始測試子彈", textStyle);
  game.add.text(410, 540, "W: 切換顯示子彈的 Body / Render", textStyle);
  // for test: list all bullets
  key_test1   = game.input.keyboard.addKey(81); // Q
  key_test2   = game.input.keyboard.addKey(87); // W

  key_test1.onDown.add(function(){
    if(!inin){
      test[0].body.x = 100;
      test[1].body.x = 120;
      test[2].body.x = 140;
      test[3].body.x = 160;
      test[4].body.x = 180;
      test[5].body.x = 200;
      test[6].body.x = 220;
      test[7].body.x = 240;
      test[8].body.x = 260;
      test[9].body.x = 280;
      for(let t of test){
        t.body.y = 300;
      }
    }
    else {
      test[0].body.x = 410;
      test[1].body.x = 430;
      test[2].body.x = 450;
      test[3].body.x = 470;
      test[4].body.x = 490;
      test[5].body.x = 510;
      test[6].body.x = 530;
      test[7].body.x = 550;
      test[8].body.x = 570;
      test[9].body.x = 590;
      for(let t of test){
        t.body.y = 580;
      }
    }
    inin = !inin;
  });

  key_test2.onDown.add(function(){
    for(let t of test){
      t.visible = !t.visible;
      t.body.debug = !t.body.debug;
    }
  });

  test[0] = StageState.bulletAGroup.getFirstDead();
  test[1] = StageState.bulletBGroup.getFirstDead();
  test[2] = StageState.bulletCGroup.getFirstDead();
  test[3] = StageState.bulletDGroup.getFirstDead();
  test[4] = StageState.bulletEGroup.getFirstDead();
  test[5] = StageState.bulletFGroup.getFirstDead();
  test[6] = StageState.bulletGGroup.getFirstDead();
  test[7] = StageState.bulletHGroup.getFirstDead();
  test[8] = StageState.bulletIGroup.getFirstDead();
  test[9] = StageState.bulletJGroup.getFirstDead();
  for(let t of test){
    t.checkWorldBounds = false;
  }
  test[0].reset(410, 580);
  test[1].reset(430, 580);
  test[2].reset(450, 580);
  test[3].reset(470, 580);
  test[4].reset(490, 580);
  test[5].reset(510, 580);
  test[6].reset(530, 580); test[6].angle = 99; test[6].body.angle = 99;
  test[7].reset(550, 580); test[7].angle = 99; test[7].body.angle = 99;
  test[8].reset(570, 580); test[8].angle = 99; test[8].body.angle = 99;
  test[9].reset(590, 580); test[9].angle = 99; test[9].body.angle = 99;
  test[0].frame = 0;
  test[1].frame = 1;
  test[2].frame = 2;
  test[3].frame = 3;
  test[4].frame = 4;
  test[5].frame = 5;
  test[6].frame = 6;
  test[7].frame = 7;
  test[8].frame = 6;
  test[9].frame = 5;
}