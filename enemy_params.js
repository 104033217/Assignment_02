
var enemy_params = {
  // youseiA: {
  //   count: 50,
  //   params: {
  //     type: "enemyA",
  //     lifecount: 1000,
  //     hp: 30,
  //     spawn: {
  //       x: 100,
  //       y: -30
  //     },
  //     item: ["t", "p", "t", "p", "t", "p", "t", "p"],
  //     s: [
  //       { count: 100, type: 6, color: 0, bullet: 0, aiming: true },
  //       { count: 140, type: 7, color: 1, bullet: 0, aiming: true },
  //       { count: 170, type: 0, color: 2, bullet: 1, aiming: true },
  //       { count: 176, type: 1, color: 2, bullet: 1, aiming: true },
  //       { count: 182, type: 2, color: 2, bullet: 1, aiming: true },
  //       { count: 188, type: 3, color: 2, bullet: 1, aiming: true },
  //       { count: 192, type: 5, color: 2, bullet: 1, aiming: true },
  //       { count: 250, type: 8, color: 3, bullet: 0 },
  //       { count: 300, type: 9, color: 4, bullet: 0 }
  //     ],
  //     v: [
  //       { count:    0, r: 700, theta:   0, ra: -24, w:   0, raa: 0.4, wa:   0 },
  //       { count:   50, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
  //       { count:  100, r:  50, theta:  90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  150, r: 200, theta: -90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  230, r: 200, theta:  90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  310, r: 200, theta: -90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  390, r: 200, theta:  90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  470, r: 200, theta: -90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  550, r: 200, theta:  90, ra:  -5, w:   0, raa:   0, wa:   0 },
  //       { count:  630, index: 3}
  //     ]
  //   }
  // },

  // youseiB: {
  //   count: 70,
  //   params: {
  //     type: "enemyB",
  //     lifecount: 500,
  //     hp: 30,
  //     spawn: {
  //       x: 450,
  //       y: 130
  //     },
  //     item: ["t", "p", "t", "p", "t", "p", "t", "p"],
  //     s: [
  //       { count: 30, type: 8, color: 6, bullet: 3, aiming: true },
  //       { count: 50, type: 7, color: 7, bullet: 2, aiming: true },
  //       { count: 57, type: 7, color: 7, bullet: 2, aiming: true },
  //       { count: 64, type: 7, color: 7, bullet: 2, aiming: true },
  //       { count: 71, type: 7, color: 7, bullet: 2, aiming: true },
  //       { count: 78, type: 7, color: 7, bullet: 2, aiming: true }
  //     ],
  //     v: [
  //       { count:    0, r: 300, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
  //       { count:   20, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
  //       { count:  100, r: 100, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
  //     ]
  //   }
  // },

  youseiC_1: {
    count: 100,
    params: {
      type: "enemyC",
      lifecount: 800,
      hp: 4,
      spawn: {
        x: 320,
        y: -30
      },
      item: ["1up", "P"],
      s: [
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  300, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:0.005 },
        { count:  400, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiC_2: {
    count: 130,
    params: {
      type: "enemyC",
      lifecount: 800,
      hp: 4,
      spawn: {
        x: 320,
        y: -30
      },
      item: ["1up", "P"],
      s: [
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  300, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:0.005 },
        { count:  400, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiC_3: {
    count: 160,
    params: {
      type: "enemyC",
      lifecount: 800,
      hp: 4,
      spawn: {
        x: 320,
        y: -30
      },
      item: ["1up", "P"],
      s: [
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  300, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:0.005 },
        { count:  400, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiC_4: {
    count: 190,
    params: {
      type: "enemyC",
      lifecount: 800,
      hp: 4,
      spawn: {
        x: 320,
        y: -30
      },
      item: ["1up", "P"],
      s: [
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  300, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:0.005 },
        { count:  400, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiC_5: {
    count: 220,
    params: {
      type: "enemyC",
      lifecount: 800,
      hp: 4,
      spawn: {
        x: 320,
        y: -30
      },
      item: ["1up", "P"],
      s: [
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  300, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:0.005 },
        { count:  400, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_1: {
    count: 280,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x:  80,
        y: -30
      },
      item: ["t", "p"],
      s: [
        { count: 200, type: 1, color: 2, bullet: 4, aiming: true }
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  250, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  340, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  400, r:  60, theta: "i", ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  490, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_2: {
    count: 310,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x:  80,
        y: -30
      },
      item: ["t", "p"],
      s: [
        { count: 200, type: 1, color: 2, bullet: 4, aiming: true }
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  250, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  340, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  400, r:  60, theta: "i", ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  490, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_3: {
    count: 340,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x:  80,
        y: -30
      },
      item: ["t", "p"],
      s: [
        { count: 200, type: 1, color: 2, bullet: 4, aiming: true }
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  250, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  340, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  400, r:  60, theta: "i", ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  490, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_4: {
    count: 370,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x:  80,
        y: -30
      },
      item: ["t", "p"],
      s: [
        { count: 200, type: 1, color: 2, bullet: 4, aiming: true }
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  250, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  340, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  400, r:  60, theta: "i", ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  490, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_5: {
    count: 400,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x:  80,
        y: -30
      },
      item: ["t", "p"],
      s: [
        { count: 200, type: 1, color: 2, bullet: 4, aiming: true}
      ],
      v: [
        { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  250, r:  60, theta:   0, ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  340, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  400, r:  60, theta: "i", ra:   0, w:  -1, raa:   0, wa:   0 },
        { count:  490, r:  60, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_6: {
    count: 480,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x: 420,
        y: 100
      },
      item: ["t", "t"],
      s: [
        { count: 50, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 53, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 59, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 320, theta:  90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r: "i", theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   90, r:  80, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_7: {
    count: 500,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x: 420,
        y: 100
      },
      item: ["t", "t"],
      s: [
        { count: 50, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 53, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 59, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 320, theta:  90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r: "i", theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   90, r:  80, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_8: {
    count: 520,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x: 420,
        y: 100
      },
      item: ["t", "t"],
      s: [
        { count: 50, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 53, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 59, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 320, theta:  90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r: "i", theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   90, r:  80, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_9: {
    count: 540,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x: 420,
        y: 100
      },
      item: ["t", "t"],
      s: [
        { count: 50, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 53, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 59, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 320, theta:  90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r: "i", theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   90, r:  80, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiA_10: {
    count: 560,
    params: {
      type: "enemyA",
      lifecount: 800,
      hp: 5,
      spawn: {
        x: 420,
        y: 100
      },
      item: ["t", "t"],
      s: [
        { count: 50, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 53, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 56, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 59, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 0, color: 3, bullet: 5, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 320, theta:  90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r: "i", theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   90, r:  80, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  youseiB_1: {
    count: 760,
    params: {
      type: "enemyB",
      lifecount: 800,
      hp: 20,
      spawn: {
        x: 100,
        y: -30
      },
      item: ["p", "t", "t", "p"],
      s: [
        { count: 60, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 61, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -10, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -20, offset_r: 0, offset_r_t: 0  },
        { count: 63, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -30, offset_r: 0, offset_r_t: 0  },
        { count: 64, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -40, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -50, offset_r: 0, offset_r_t: 0  },
        { count: 66, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -60, offset_r: 0, offset_r_t: 0  },
        { count: 67, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -70, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -80, offset_r: 0, offset_r_t: 0  },
        { count: 69, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -90, offset_r: 0, offset_r_t: 0  },
        { count: 70, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -100, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -110, offset_r: 0, offset_r_t: 0  },
        { count: 72, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -120, offset_r: 0, offset_r_t: 0  },
        { count: 73, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -130, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -140, offset_r: 0, offset_r_t: 0  },
        { count: 75, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -150, offset_r: 0, offset_r_t: 0  },
        { count: 76, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -160, offset_r: 0, offset_r_t: 0  },
        { count: 77, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -170, offset_r: 0, offset_r_t: 0  },
        { count: 78, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -180, offset_r: 0, offset_r_t: 0  },
        { count: 79, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -190, offset_r: 0, offset_r_t: 0  },
        { count: 80, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -200, offset_r: 0, offset_r_t: 0  },
        { count: 81, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -210, offset_r: 0, offset_r_t: 0  },
        { count: 82, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -220, offset_r: 0, offset_r_t: 0  },
        { count: 83, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -230, offset_r: 0, offset_r_t: 0  },
        { count: 84, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -240, offset_r: 0, offset_r_t: 0  },
        { count: 85, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -250, offset_r: 0, offset_r_t: 0  },
        { count: 86, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -260, offset_r: 0, offset_r_t: 0  },
        { count: 87, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -270, offset_r: 0, offset_r_t: 0  },
        { count: 88, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -280, offset_r: 0, offset_r_t: 0  },
        { count: 89, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -290, offset_r: 0, offset_r_t: 0  },
        { count: 90, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -300, offset_r: 0, offset_r_t: 0  },
        { count: 91, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -310, offset_r: 0, offset_r_t: 0  },
        { count: 92, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -320, offset_r: 0, offset_r_t: 0  },
        { count: 93, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -330, offset_r: 0, offset_r_t: 0  },
        { count: 94, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -340, offset_r: 0, offset_r_t: 0  },
        { count: 95, type: 1, color: 2, bullet: 5, aiming: false, offset_t: -350, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 450, theta:   0, ra: -13, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  170, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },
  youseiB_2: {
    count: 860,
    params: {
      type: "enemyB",
      lifecount: 800,
      hp: 20,
      spawn: {
        x: 300,
        y: -30
      },
      item: ["p", "t", "t", "p"],
      s: [
        { count: 60, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 0, offset_r: 0, offset_r_t: 0  },
        { count: 61, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 10, offset_r: 0, offset_r_t: 0  },
        { count: 62, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 20, offset_r: 0, offset_r_t: 0  },
        { count: 63, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 30, offset_r: 0, offset_r_t: 0  },
        { count: 64, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 40, offset_r: 0, offset_r_t: 0  },
        { count: 65, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 50, offset_r: 0, offset_r_t: 0  },
        { count: 66, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 60, offset_r: 0, offset_r_t: 0  },
        { count: 67, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 70, offset_r: 0, offset_r_t: 0  },
        { count: 68, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 80, offset_r: 0, offset_r_t: 0  },
        { count: 69, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 90, offset_r: 0, offset_r_t: 0  },
        { count: 70, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 100, offset_r: 0, offset_r_t: 0  },
        { count: 71, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 110, offset_r: 0, offset_r_t: 0  },
        { count: 72, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 120, offset_r: 0, offset_r_t: 0  },
        { count: 73, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 130, offset_r: 0, offset_r_t: 0  },
        { count: 74, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 140, offset_r: 0, offset_r_t: 0  },
        { count: 75, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 150, offset_r: 0, offset_r_t: 0  },
        { count: 76, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 160, offset_r: 0, offset_r_t: 0  },
        { count: 77, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 170, offset_r: 0, offset_r_t: 0  },
        { count: 78, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 180, offset_r: 0, offset_r_t: 0  },
        { count: 79, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 190, offset_r: 0, offset_r_t: 0  },
        { count: 80, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 200, offset_r: 0, offset_r_t: 0  },
        { count: 81, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 210, offset_r: 0, offset_r_t: 0  },
        { count: 82, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 220, offset_r: 0, offset_r_t: 0  },
        { count: 83, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 230, offset_r: 0, offset_r_t: 0  },
        { count: 84, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 240, offset_r: 0, offset_r_t: 0  },
        { count: 85, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 250, offset_r: 0, offset_r_t: 0  },
        { count: 86, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 260, offset_r: 0, offset_r_t: 0  },
        { count: 87, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 270, offset_r: 0, offset_r_t: 0  },
        { count: 88, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 280, offset_r: 0, offset_r_t: 0  },
        { count: 89, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 290, offset_r: 0, offset_r_t: 0  },
        { count: 90, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 300, offset_r: 0, offset_r_t: 0  },
        { count: 91, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 310, offset_r: 0, offset_r_t: 0  },
        { count: 92, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 320, offset_r: 0, offset_r_t: 0  },
        { count: 93, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 330, offset_r: 0, offset_r_t: 0  },
        { count: 94, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 340, offset_r: 0, offset_r_t: 0  },
        { count: 95, type: 1, color: 2, bullet: 5, aiming: false, offset_t: 350, offset_r: 0, offset_r_t: 0  }
      ],
      v: [
        { count:    0, r: 450, theta:   0, ra: -13, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  170, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },
  
  youseiB_3: {
    count: 980,
    params: {
      type: "enemyC",
      lifecount: 1000,
      hp: 220,
      spawn: {
        x: 200,
        y: -30
      },
      item: ["p", "t", "t", "p", "P", "t", "t"],
      s: [
        { count: 70, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 70, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -90  },
        { count: 80, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 80, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -90  },
        { count: 90, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 90, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -90  },
        { count: 100, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  },
        { count: 100, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -90  },
        { count: 110, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  },
        { count: 110, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -90  },
        { count: 120, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  },
        { count: 120, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -90  },
        { count: 130, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 110  },
        { count: 130, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -70  },
        { count: 140, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 110  },
        { count: 140, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -70  },
        { count: 150, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 110  },
        { count: 150, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -70  },
        { count: 160, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 110  },
        { count: 160, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -70  },
        { count: 170, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 110  },
        { count: 170, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -70  },
        { count: 180, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 110  },
        { count: 180, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -70  },
        { count: 190, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 130  },
        { count: 190, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -50  },
        { count: 200, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 130  },
        { count: 200, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -50  },
        { count: 210, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 130  },
        { count: 210, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -50  },
        { count: 220, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 130  },
        { count: 220, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -50  },
        { count: 230, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 130  },
        { count: 230, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -50  },
        { count: 240, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 130  },
        { count: 240, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -50  },
        { count: 250, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 150  },
        { count: 250, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -30  },
        { count: 260, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 150  },
        { count: 260, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -30  },
        { count: 270, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 150  },
        { count: 270, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -30  },
        { count: 280, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 150  },
        { count: 280, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -30  },
        { count: 290, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 150  },
        { count: 290, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -30  },
        { count: 300, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 150  },
        { count: 300, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -30  },
        { count: 310, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 170  },
        { count: 310, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -10  },
        { count: 320, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 170  },
        { count: 320, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -10  },
        { count: 330, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 170  },
        { count: 330, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: -10  },
        { count: 340, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 170  },
        { count: 340, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -10  },
        { count: 350, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 170  },
        { count: 350, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -10  },
        { count: 360, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 170  },
        { count: 360, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: -10  },
        { count: 370, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 190  },
        { count: 370, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 10  },
        { count: 380, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 190  },
        { count: 380, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 10  },
        { count: 390, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 190  },
        { count: 390, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 10  },
        { count: 400, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 190  },
        { count: 400, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 10  },
        { count: 410, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 190  },
        { count: 410, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 10  },
        { count: 420, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 190  },
        { count: 420, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 10  },
        { count: 430, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 210  },
        { count: 430, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 30  },
        { count: 440, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 210  },
        { count: 440, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 30  },
        { count: 450, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 210  },
        { count: 450, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 30  },
        { count: 460, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 210  },
        { count: 460, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 30  },
        { count: 470, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 210  },
        { count: 470, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 30  },
        { count: 480, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 210  },
        { count: 480, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 30  },
        { count: 490, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 230  },
        { count: 490, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 50  },
        { count: 500, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 230  },
        { count: 500, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 50  },
        { count: 510, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 230  },
        { count: 510, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 50  },
        { count: 520, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 230  },
        { count: 520, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 50  },
        { count: 530, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 230  },
        { count: 530, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 50  },
        { count: 540, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 230  },
        { count: 540, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 50  },
        { count: 550, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 250  },
        { count: 550, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 70  },
        { count: 560, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 250  },
        { count: 560, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 70  },
        { count: 570, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 250  },
        { count: 570, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 70  },
        { count: 580, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 250  },
        { count: 580, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 70  },
        { count: 590, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 250  },
        { count: 590, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 70  },
        { count: 600, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 250  },
        { count: 600, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 70  },
        { count: 610, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 270  },
        { count: 610, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 620, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 270  },
        { count: 620, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 630, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 270  },
        { count: 630, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 0, offset_r: 90, offset_r_t: 90  },
        { count: 640, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 270  },
        { count: 640, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  },
        { count: 650, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 270  },
        { count: 650, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  },
        { count: 660, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 270  },
        { count: 660, type: 5, color: 0, bullet: 6, aiming: true, offset_t: 30, offset_r: 90, offset_r_t: 90  }
      ],
      v: [
        { count:    0, r: 450, theta:   0, ra: -13, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  700, r:  60, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },
  
  youseiB_4: {
    count: 1100,
    params: {
      type: "enemyB",
      lifecount: 800,
      hp: 220,
      spawn: {
        x: 110,
        y: -30
      },
      item: ["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"],
      s: [
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, }
      ],
      v: [
        { count:    0, r: 450, theta:   0, ra: -10, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  320, r:  60, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },
  youseiB_5: {
    count: 1125,
    params: {
      type: "enemyB",
      lifecount: 800,
      hp: 15,
      spawn: {
        x: 290,
        y: -30
      },
      item: ["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"],
      s: [
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 50, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 100, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 150, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 200, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 250, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 9, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: 3, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -3, },
        { count: 300, type: 7, color: 4, bullet: 7, aiming: true, offset_t: -9, }
      ],
      v: [
        { count:    0, r: 450, theta:   0, ra: -10, w:   0, raa: 0.1, wa:   0 },
        { count:   40, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  320, r:  60, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 }
      ]
    }
  },

  boss_1: {
    count: 1700,
    params: {
      boss: 1,
      type: "dot_eirin",
      lifecount: 1000,
      hp: 200,
      spawn: {
        x: 200,
        y: -30
      },
      item: ["s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "P"],
      s: [
        { count: 160, type: 8, color: 5, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 175, type: 8, color: 0, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 230, type: 8, color: 5, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 245, type: 8, color: 0, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 280, type: 8, color: 5, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 295, type: 8, color: 0, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 330, type: 8, color: 5, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 345, type: 8, color: 0, bullet: 9, aiming: true, offset_t: 0, offset_r: 0, offset_r_t: 0 },
        { count: 410, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 411, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 412, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 413, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 414, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 415, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 416, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 417, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 418, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 419, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 420, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 421, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 422, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 423, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 424, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 425, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 426, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 427, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 428, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 429, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 430, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 431, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 432, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 433, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 434, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 435, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 436, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 437, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 438, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 439, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 440, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 441, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 442, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 443, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 444, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 445, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 446, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 447, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 448, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 449, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 450, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 451, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 452, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 453, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 454, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 455, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 456, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 457, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 458, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 459, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 460, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 461, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 462, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 463, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 464, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 465, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 466, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 467, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 468, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 469, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 470, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 471, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 472, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 473, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 474, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 475, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 476, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 477, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 478, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 479, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 480, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 481, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 482, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 483, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 484, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 485, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 486, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 487, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 488, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 489, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 490, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 491, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 492, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 493, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 494, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 495, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 496, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 497, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 498, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 499, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 500, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 501, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 502, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 503, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 504, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 505, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 506, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 507, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 508, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 509, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 510, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 511, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 512, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 513, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 514, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 515, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 516, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 517, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 518, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 519, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 520, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 521, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 522, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 523, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 524, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 525, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 526, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 527, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 528, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 529, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 530, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 531, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 532, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 533, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 534, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 535, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 536, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 537, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 538, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 539, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 540, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 541, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 542, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 543, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 544, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 545, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 546, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 547, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 548, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 549, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 550, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 551, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 552, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 553, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 554, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 555, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 556, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 557, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 558, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 559, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 560, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 561, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 562, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 563, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 564, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 565, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 566, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 567, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 568, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 569, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 570, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 571, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 572, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 573, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 574, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 575, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 576, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 577, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 578, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 579, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 580, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 581, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 582, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 583, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 584, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 585, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 586, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 587, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 588, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
        { count: 589, type: 9, color: 5, bullet: 8, aiming: false, offset_t: "r", offset_r: 0, offset_r_t: 0, se_depress: true },
      ],
      v: [
        { count:    0, r: 200, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:   50, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0, debug: true },
        { count:  100, r: 360, theta: -80, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:  140, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  161, r: 480, theta: 110, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:  220, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  231, r: 480, theta: -90, ra:  -9, w:   0, raa: 0.1, wa:   0 },
        { count:  270, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  281, r: 540, theta:  75, ra:  -9, w:   0, raa: 0.1, wa:   0, debug: true },
        { count:  320, r:   0, theta:  75, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  331, r: 402, theta:-89.3, ra:  -8, w:   0, raa: 0.1, wa:   0 },
        { count:  360, r:   0, theta:-89.3, ra:   0, w:   0, raa:   0, wa:   0, debug: true },
        { count:  590, index: 2, shotRollback: true }
      ]
    }
  },
  boss_2: {
    params: {
      boss: 1,
      type: "dot_eirin",
      lifecount: 1000,
      hp: 200,
      spawn: {
        x: 200,
        y: 100
      },
      item: ["s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "P"],
      s: [
        { count: 100, type: 8, color: 1, bullet: 10, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 120, type: 8, color: 3, bullet: 11, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 140, type: 8, color: 1, bullet: 10, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 160, type: 8, color: 3, bullet: 11, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 180, type: 8, color: 1, bullet: 10, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 200, type: 8, color: 3, bullet: 11, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 220, type: 8, color: 1, bullet: 10, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 240, type: 8, color: 3, bullet: 11, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },

        { count: 320, type: 7, color: 7, bullet: 12, aiming: false, offset_t: "rs", offset_r: 210, offset_r_t: -114 },
        { count: 320, type: 7, color: 6, bullet: 13, aiming: false, offset_t: "rs", offset_r: 520, offset_r_t: -21 },
        { count: 320, type: 7, color: 2, bullet: 14, aiming: false, offset_t: "rs", offset_r: 520, offset_r_t: 21 },
        { count: 320, type: 7, color: 0, bullet: 15, aiming: false, offset_t: "rs", offset_r: 210, offset_r_t: 114 },
        
        { count: 400, type: 8, color: 4, bullet: 10, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 400, type: 8, color: 6, bullet: 11, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 420, type: 7, color: 7, bullet: 12, aiming: false, offset_t: "rs", offset_r: 210, offset_r_t: -114 },
        { count: 420, type: 7, color: 6, bullet: 13, aiming: false, offset_t: "rs", offset_r: 520, offset_r_t: -21 },
        { count: 420, type: 7, color: 2, bullet: 14, aiming: false, offset_t: "rs", offset_r: 520, offset_r_t: 21 },
        { count: 420, type: 7, color: 0, bullet: 15, aiming: false, offset_t: "rs", offset_r: 210, offset_r_t: 114 }
      ],
      v: [
        { count:    0, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:    1, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  550, index: 1, shotRollback: true }
      ]
    }
  },
  boss_3: {
    params: {
      boss: 2,
      type: "dot_eirin",
      lifecount: 1000,
      hp: 300,
      spawn: {
        x: 200,
        y: 300
      },
      item: ["s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s", "s"],
      s: [
        // fps壓力測試
        { count: 200, type: 5, color: 1, bullet: 16, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 290, type: 1, color: 2, bullet: 17, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 380, type: 8, color: 6, bullet: 16, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
        { count: 470, type: 9, color: 0, bullet: 17, aiming: false, offset_t: "rs", offset_r: 0, offset_r_t: 0 },
      ],
      v: [
        { count:    0, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  110, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
        { count:  471, index: 1, shotRollback: true }
      ]
    }
  }



}
